#pragma once
#include <opencv2/opencv.hpp>
#include <k4a/k4a.h>
#include <pcl/common/common.h>
#include <unordered_map>

// Definition of 32bit k4a::COLOR_BGRA pixel datatype
struct bgra_t {
	uint8_t b;
	uint8_t g;
	uint8_t r;
	uint8_t a;
};

// Creates look table for undistorting depth map. 
// Per: https://github.com/microsoft/Azure-Kinect-Sensor-SDK/tree/develop/examples/fastpointcloud
void create_xy_table(
	const k4a_calibration_t* calibration,
	k4a_image_t xy_table,
	int* valid_size);

// Undistorts the depth map using the xy lookup table into the pcl cloud object
int generate_pcl_cloud(
	const k4a_image_t depth_image,
	const k4a_image_t xy_table,
	const k4a_image_t color_image,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud,
	bool isStructured = true);

// Creates [x,y] undistortion maps using opencv with Brown's Conrady model from opencv
void InitUndistortionMapsCV(
	const k4a_calibration_camera_t i,
	cv::Mat& xMap,
	cv::Mat& yMap);

// Undistorts the depth map using the opencv undistortion maps into a buffer
int MapDepthToPointCloudCV(
	const cv::Mat& depthData,
	const k4a_calibration_camera_t calibration,
	float* cloudBuffer);

// Wraps kinect camera calibraiton coefficients as opencv mat
cv::Mat GetCameraMatrixFromK4A(
	const k4a_calibration_camera_t cal);

// Wraps kinect camera distortion coefficients as opencv mat
cv::Mat GetDistortionMatrixFromK4A(
	const k4a_calibration_camera_t cal);

// Wrap kinect image as opencv mat
// TODO: Check performance or inlining
cv::Mat ImageKinect2CV(
	const k4a_image_t kinect_image);

// Mapping of common image types between K4A and OpenCV libraries
const std::unordered_map<k4a_image_format_t, int> TYPE_K4A_TO_CV {

	{K4A_IMAGE_FORMAT_COLOR_NV12,	CV_8UC3},      // TODO: Check this
	{K4A_IMAGE_FORMAT_COLOR_YUY2,	CV_8UC2},      // TODO: Check this
	{K4A_IMAGE_FORMAT_COLOR_BGRA32, CV_8UC4},    
	{K4A_IMAGE_FORMAT_DEPTH16,		CV_16UC1},
	{K4A_IMAGE_FORMAT_IR16,			CV_16UC1},
	{K4A_IMAGE_FORMAT_CUSTOM8 ,		CV_8UC1},
	{K4A_IMAGE_FORMAT_CUSTOM16,		CV_16UC1},
	{K4A_IMAGE_FORMAT_CUSTOM,		-1}            // Unknown type
};

// Transform eigen matrix between kinect spaces
void TransformMatrix(
	Eigen::Matrix4f& transform,
	k4a_calibration_t& device_calibration,
	k4a_calibration_type_t from,
	k4a_calibration_type_t to);

