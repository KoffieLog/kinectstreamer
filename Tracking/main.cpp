#include "pch.h"	
#include "main.h"


int main() {

	// Set log level for opencv
	cv::utils::logging::setLogLevel(cv::utils::logging::LogLevel::LOG_LEVEL_SILENT);

	// Start kinect camera
	k4a_device_t device;
	k4a_calibration_t device_calibration;
	if (EnableK4ACamera(&device, &device_calibration) == 1) {
		return 1;
	}

	// Init k4a members
	k4a_image_t xy_buffer = NULL;
	k4a_image_t rgbd_buffer = NULL;
	k4a_transformation_t transformation = NULL;
	if (!InitK4A(device, device_calibration, &transformation, &xy_buffer, &rgbd_buffer)) {
		return 1;
	}

	// Get camera params
	auto k = GetCameraMatrixFromK4A(device_calibration.color_camera_calibration);
	auto d = GetDistortionMatrixFromK4A(device_calibration.color_camera_calibration);
	CameraParams rgb_intrinsics{ k, d };

	// Init pcl pointcloud
	pcl::PointCloud<pcl::PointXYZRGB >::Ptr pcl_cloud(new pcl::PointCloud<pcl::PointXYZRGB >);
	pcl_cloud->width = device_calibration.depth_camera_calibration.resolution_width;
	pcl_cloud->height = device_calibration.depth_camera_calibration.resolution_height;
	pcl_cloud->resize(static_cast<size_t>(pcl_cloud->width) * pcl_cloud->height);
	pcl_cloud->is_dense = false;

	// Create qr detector
	apriltag_family_t* tf = nullptr;
	apriltag_detector_t* april_detector = nullptr;
	CreateAprilDetector(&tf, &april_detector);

	// Point cloud viewer
	pcl::visualization::PCLVisualizer::Ptr pcl_viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
	pcl_viewer->getRenderWindow()->GlobalWarningDisplayOff();
	pcl_viewer->initCameraParameters();
	pcl_viewer->setCameraPosition(0, 0, -1000, 0, -1, 0); // 1 meter behind camera looking forward
	pcl_viewer->addCoordinateSystem(150);				  // 150mm coordinate system at [0,0,0]

	// Vars
	std::string logFolder;
	bool isLogging = false;
	bool exit = false;

	// Keyboard listener
	AsyncKeyListener keyListener;
	keyListener.RegisterCallback(VK_ESCAPE, [&exit]() { exit = true; });
	keyListener.RegisterCallback(0x52, [&isLogging, &logFolder, &device, &device_calibration]() {  // 0x52 == 'R'
		isLogging = !isLogging;
	if (isLogging) { logFolder = CreateDataFolder("Data", true, device, device_calibration); }
		});

	while (!exit && !pcl_viewer->wasStopped()) {

		// Capture sensor data
		k4a_capture_t capture;
		k4a_wait_result_t get_capture_result = k4a_device_get_capture(device, &capture, K4A_WAIT_INFINITE);
		if (get_capture_result != K4A_WAIT_RESULT_SUCCEEDED) {
			continue;
		}

		// Unpack RGB data
		cv::Mat cv_color;
		ProcessColorImage(capture, cv_color);

		// Unpack IR data
		cv::Mat cv_ir, cv_tof;
		ProcessIRImage(capture, cv_ir, cv_tof);

		// Compute XYZ data
		ProcessPointCloud(capture, transformation, xy_buffer, rgbd_buffer, pcl_cloud);

		// April tag detection
		std::vector<DetectionResult<Eigen::Matrix<float, 2, 4>>> april_corners;                                    // container to hold detected april tag corners
		std::vector<DetectionResult<Eigen::Matrix<float, 4, 4>>> april_detections_rgb;                             // container to hold detected april tag 3d transforms (color camera space)
		DetectAprilTag3D(april_detector, cv_color, rgb_intrinsics, &april_corners, &april_detections_rgb);

		// Transform detections from color to depth space
		std::vector<DetectionResult<Eigen::Matrix<float, 4, 4>>> april_detections_depth;                           // container to hold detected april tag 3d transforms (depth camera space)
		for (auto det : april_detections_rgb) {
			TransformMatrix(det.matrix, device_calibration, K4A_CALIBRATION_TYPE_COLOR, K4A_CALIBRATION_TYPE_DEPTH);
			april_detections_depth.emplace_back(det.id, det.matrix);
		}

		// Dump all data to disk and add red dot to color image to show recording
		if (isLogging) {
			LogData(logFolder, cv_color, cv_ir, cv_tof, pcl_cloud, april_detections_rgb, april_detections_depth);
			cv::circle(cv_color, cv::Point(25, 25), 5, cv::Scalar(0, 0, 255, 0), -1);
			cv::circle(cv_color, cv::Point(25, 25), 9, cv::Scalar(0, 0, 255, 0), +2);
		}

		// Draw 2D / 3D pose onto RGB image
		for (const auto& det : april_corners) {
			DrawAprilDetection(cv_color, det.id, det.matrix);
		}
		for (const auto& det : april_detections_rgb) {
			DrawPose2D(cv_color, det.matrix, rgb_intrinsics);
		}

		// Update point cloud in viewer
		pcl_viewer->removeAllPointClouds();
		pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> pcl_cloud_rgb(pcl_cloud);
		pcl_viewer->addPointCloud<pcl::PointXYZRGB>(pcl_cloud, pcl_cloud_rgb);
		pcl_viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2);

		// Add all detected 3D poses to point cloud viewer
		pcl_viewer->removeAllShapes();
		for (const auto& det : april_detections_depth) {
			DrawPose3D(pcl_viewer, det.matrix, det.id);
		}

		// Release capture data
		k4a_capture_release(capture);

		// Spin viewers
		cv::imshow("Color", cv_color);
		cv::imshow("IR", cv_ir);
		pcl_viewer->spinOnce(1);
		cv::waitKey(1);
	}

	k4a_device_stop_cameras(device);
	k4a_device_stop_imu(device);
	k4a_device_close(device);
	apriltag_detector_destroy(april_detector);
	tag36h11_destroy(tf);
}

void DrawPose3D(
	pcl::visualization::PCLVisualizer::Ptr visualizer,
	Eigen::Matrix4f transform,
	int id = 0) {

	// Scale direction to length and shift
	float l = 150.0f;
	transform.block<3, 3>(0, 0) *= l;
	transform.block<3, 3>(0, 0).colwise() += transform.block<3, 1>(0, 3);

	// Construct pcl points
	pcl::PointXYZ x(transform(0, 0), transform(1, 0), transform(2, 0));
	pcl::PointXYZ y(transform(0, 1), transform(1, 1), transform(2, 1));
	pcl::PointXYZ z(transform(0, 2), transform(1, 2), transform(2, 2));
	pcl::PointXYZ o(transform(0, 3), transform(1, 3), transform(2, 3));

	// Add coordinate system
	visualizer->addLine<pcl::PointXYZ>(o, x, 255, 0, 0, "xDir_" + std::to_string(id));
	visualizer->addLine<pcl::PointXYZ>(o, y, 0, 255, 0, "yDir_" + std::to_string(id));
	visualizer->addLine<pcl::PointXYZ>(o, z, 0, 0, 255, "zDir_" + std::to_string(id));
}

bool InitK4A(
	k4a_device_t device,
	k4a_calibration_t device_calibration,
	k4a_transformation_t* device_transform,
	k4a_image_t* xy_buffer,
	k4a_image_t* rgbd_buffer) {

	// Get transform object from calibraiton
	*device_transform = k4a_transformation_create(&device_calibration);

	// Init result
	k4a_result_t result;

	// Create img buffers
	result = k4a_image_create(
		k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_BGRA32,
		device_calibration.depth_camera_calibration.resolution_width,
		device_calibration.depth_camera_calibration.resolution_height,
		device_calibration.depth_camera_calibration.resolution_width * (int)sizeof(bgra_t),
		rgbd_buffer);

	// Check result
	if (K4A_FAILED(result))
		return false;

	result = k4a_image_create(
		k4a_image_format_t::K4A_IMAGE_FORMAT_CUSTOM,
		device_calibration.depth_camera_calibration.resolution_width,
		device_calibration.depth_camera_calibration.resolution_height,
		device_calibration.depth_camera_calibration.resolution_width * (int)sizeof(k4a_float2_t),
		xy_buffer);

	// Check result
	if (K4A_FAILED(result))
		return false;

	int xySize = 0;
	create_xy_table(&device_calibration, *xy_buffer, &xySize);

	return true;
}

std::string CreateDataFolder(
	std::string folder_name,
	bool add_timestamp,
	k4a_device_t device,
	k4a_calibration_t device_Calibration) {

	if (add_timestamp) {

		// Millisecond timestamp
		auto epoch = std::chrono::system_clock::now().time_since_epoch();
		auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(epoch).count();
		folder_name += "_" + std::to_string(ms);
	}

	// Create a folder for recordings
	std::error_code e;
	if (!std::filesystem::create_directory(folder_name, e)) {
		std::cout << e << std::endl;
	}

	// Get device info and dump to file 
	size_t serial_size = 0;
	k4a_device_get_raw_calibration(device, NULL, &serial_size);
	uint8_t* cal = (uint8_t*)(malloc(serial_size));
	k4a_device_get_raw_calibration(device, cal, &serial_size);

	std::ofstream ofs(folder_name + "/device_info.json", std::ofstream::out);
	ofs << cal << std::endl;
	ofs.close();
	free(cal);

	return folder_name;
}

int ProcessPointCloud(
	k4a_capture_t& capture,
	k4a_transformation_t transformation,
	k4a_image_t xy_buffer,
	k4a_image_t rgbd_buffer,
	pcl::PointCloud<pcl::PointXYZRGB >::Ptr pcl_cloud) {

	// Get access to image buffers
	k4a_image_t color_image = k4a_capture_get_color_image(capture);
	k4a_image_t depth_image = k4a_capture_get_depth_image(capture);

	// Transform rgb to depth image
	k4a_transformation_color_image_to_depth_camera(transformation, depth_image, color_image, rgbd_buffer);

	// Project depth to XYZ-RGB
	int valid_count = generate_pcl_cloud(depth_image, xy_buffer, rgbd_buffer, pcl_cloud);

	k4a_image_release(depth_image);
	k4a_image_release(color_image);

	return valid_count;
}

void LogData(
	std::string folder,
	cv::Mat rgb,
	cv::Mat ir,
	cv::Mat tof,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
	std::vector<DetectionResult<Eigen::Matrix4f>> detections_rgb,
	std::vector<DetectionResult<Eigen::Matrix4f>> detections_depth) {

	// Create timestamp
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	auto timeStamp = std::to_string(ms);

	// Write rgb and ir data
	cv::imwrite(folder + "/rgb_" + timeStamp + ".png", rgb);   // 3 x uint_8
	cv::imwrite(folder + "/ir_" + timeStamp + ".pgm", ir);     // 1 x uint_8
	cv::imwrite(folder + "/tof_" + timeStamp + ".pgm", tof);   // 1 x uint_16

	// Output filestream
	std::ofstream ofs;

	// Write rgb tracker data
	ofs.open(folder + "/transforms_rgb_" + timeStamp + ".csv");
	for (const auto& det : detections_rgb) {
		auto s = FormatTransformToString(det.matrix);
		ofs.write(s.c_str(), (std::streamsize)s.length());
	}
	ofs.close();

	// Write depth tracker data
	ofs.open(folder + "/transforms_depth_" + timeStamp + ".csv");
	for (const auto& det : detections_depth) {
		auto s = FormatTransformToString(det.matrix);
		ofs.write(s.c_str(), (std::streamsize)s.length());
	}
	ofs.close();

	// Encode point cloud data
	unsigned nValid = 0;
	std::ostringstream ply_data;
	for (size_t i = 0; i < cloud->size(); i++) {
		if (pcl::isFinite(cloud->points[i])) {
			ply_data << cloud->points[i].x << ' ' << cloud->points[i].y << ' ' << cloud->points[i].z << ' ';
			ply_data << (unsigned)cloud->points[i].r << ' ' << (unsigned)cloud->points[i].g << ' ' << (unsigned)cloud->points[i].b << '\n';
			nValid++;
		}
	}

	// Encode point cloud header
	std::ostringstream ply_header;
	ply_header <<
		"ply" << '\n' <<
		"format ascii 1.0" << '\n' <<
		"element vertex " << nValid << '\n' <<
		"property float x" << '\n' <<
		"property float y" << '\n' <<
		"property float z" << '\n' <<
		"property uchar red" << '\n' <<
		"property uchar green" << '\n' <<
		"property uchar blue" << '\n' <<
		"end_header" << std::endl;

	// Dump pc data to file
	ofs.open(folder + "/pc_" + timeStamp + ".ply");
	ofs.write(ply_header.str().c_str(), (std::streamsize)ply_header.str().length());  // write header
	ofs.write(ply_data.str().c_str(), (std::streamsize)ply_data.str().length());      // write point cloud
	ofs.close();
}

void CreateAprilDetector(
	apriltag_family_t** _tf,
	apriltag_detector_t** _td) {

	// Initialize tag detector with options
	auto tf = tag36h11_create();

	auto td = apriltag_detector_create();
	apriltag_detector_add_family(td, tf);

	// Detection params
	td->quad_decimate = 2.0;
	td->quad_sigma = 3.0;
	td->nthreads = 10;
	td->debug = false;
	td->refine_edges = true;
	td->decode_sharpening = 0.25;

	// Set out params
	*_tf = tf;
	*_td = td;
}

int EnableK4ACamera(
	k4a_device_t* device,
	k4a_calibration_t* device_calibration) {

	// Configure a kinect stream
	k4a_device_configuration_t config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
	config.synchronized_images_only = true;                                        // rgb and ir/tof images will be in sync
	config.camera_fps = k4a_fps_t::K4A_FRAMES_PER_SECOND_15;                       // frame rate
	config.color_format = k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_BGRA32;       // color format (dont change)
	config.color_resolution = k4a_color_resolution_t::K4A_COLOR_RESOLUTION_720P;   // resolution of rgb camera
	config.depth_mode = k4a_depth_mode_t::K4A_DEPTH_MODE_WFOV_2X2BINNED;           // field of view and binning of tof sensor
	config.disable_streaming_indicator = false;                                     // enable or disable white led on kinect front plate

	// Get number of connected kinect devices
	uint32_t device_count = k4a_device_get_installed_count();
	if (device_count == 0) {
		printf("No k4a devices attached!\n");
		return 1;
	}

	// Open first kinect device (index = 0)
	if (K4A_FAILED(k4a_device_open(0, device))) {
		printf("Failed to open k4a device!\n");
		return 1;
	}

	// Start the cameras with the given configuration
	if (K4A_FAILED(k4a_device_start_cameras(*device, &config))) {
		printf("Failed to start cameras!\n");
		k4a_device_close(*device);
		return 1;
	}

	// Start the IMU
	if (K4A_FAILED(k4a_device_start_imu(*device))) {
		printf("Failed to start IMU!\n");
		k4a_device_close(*device);
		return 1;
	}

	// Get device calibraiton object
	if (K4A_FAILED(k4a_device_get_calibration(*device, config.depth_mode, config.color_resolution, device_calibration))) {
		printf("Failed to start cameras!\n");
		k4a_device_close(*device);
		return false;
	};

	return 0;
}

void ProcessColorImage(
	k4a_capture_t& capture,
	cv::Mat& img) {

	k4a_image_t colorImage = k4a_capture_get_color_image(capture);

	// COLOR
	if (colorImage != nullptr)
	{
		// Wrap color image as cv mat object
		img = ImageKinect2CV(colorImage);

		// Release resc
		k4a_image_release(colorImage);
	}
}

void ProcessIRImage(
	k4a_capture_t& capture,
	cv::Mat& out_ir,
	cv::Mat& out_tof,
	bool equalise_ir) {

	k4a_image_t infrared_Image = k4a_capture_get_ir_image(capture);
	k4a_image_t depth_image = k4a_capture_get_depth_image(capture);

	if (infrared_Image)
	{
		// Wrap depth/ir map as cv mat objects for easy debugging
		out_ir = ImageKinect2CV(infrared_Image);
		
		// Histogram eq
		if (equalise_ir) {

			uint16_t val_95;
			Percentile<uint16_t, 256, 1, 65535>((uint16_t*)out_ir.data, out_ir.rows * out_ir.cols, 1, 95, nullptr, &val_95);
			double alpha = 255.0 / (val_95 - 1.0);
			double beta = -alpha;
			out_ir.convertTo(out_ir, CV_8U, alpha, beta);
		}
		
		// Release image data
		k4a_image_release(infrared_Image);
	}

	if (depth_image)
	{
		// Wrap depth/ir map as cv mat objects for easy debugging
		out_tof = ImageKinect2CV(depth_image);

		// Release image data
		k4a_image_release(depth_image);
	}
}

void DetectIRMarker(
	cv::Mat img) {

	// Detect IR Markers
	std::vector<cv::Point2f> detections;
	//ETRO::IRTracking::IRDetector::Detect_GA(img, detections);

	// Draw all ir markers detections
	for (auto det : detections) {
		cv::drawMarker(img, det, cv::Scalar(255, 0, 255, 255), cv::MARKER_CROSS, 20);
	}

}

void DetectAprilTag3D(
	apriltag_detector_t* td,
	cv::Mat& img,
	CameraParams& intrinsics,
	std::vector<DetectionResult<Eigen::Matrix<float, 2, 4>>>* out_corners,
	std::vector<DetectionResult<Eigen::Matrix<float, 4, 4>>>* out_transforms) {

	// Convert to grayscale
	cv::Mat cv_gray;
	cv::cvtColor(img, cv_gray, cv::COLOR_BGR2GRAY);

	// Make an image_u8_t header for the Mat data
	image_u8_t im{ cv_gray.cols, cv_gray.rows, cv_gray.cols, cv_gray.data };

	// Detect tags in image
	zarray_t* detections = apriltag_detector_detect(td, &im);

	// Draw detection outlines
	for (int i = 0; i < zarray_size(detections); i++) {

		// Get detected marker
		apriltag_detection_t* det;
		zarray_get(detections, i, &det);

		// Points in image (2D)
		cv::Mat i_pts = (cv::Mat_<double>(4, 2) <<
			det->p[3][0], det->p[3][1],
			det->p[2][0], det->p[2][1],
			det->p[1][0], det->p[1][1],
			det->p[0][0], det->p[0][1]);

		// Points in object (3D)
		const float s = 20.4 * 4;
		cv::Mat o_pts = (cv::Mat_<float>(4, 3) <<
			-s, +s, 0,
			+s, +s, 0,
			+s, -s, 0,
			-s, -s, 0);

		// Get 3d pose
		cv::Mat rVec, tVec;
		cv::solvePnP(o_pts, i_pts, intrinsics.cameraMatrix, intrinsics.distortionCoefficients, rVec, tVec, false, cv::SOLVEPNP_IPPE_SQUARE);

		// Construct 4x4 transform
		cv::Rodrigues(rVec, rVec);
		Eigen::Transform<double, 3, 0> T;
		Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
		transform.block<3, 3>(0, 0) = Eigen::Map<Eigen::Matrix3d>((double*)rVec.data).cast<float>().transpose();
		transform.block<3, 1>(0, 3) = Eigen::Map<Eigen::Vector3d>((double*)tVec.data).cast<float>();

		// Save to result containers
		out_transforms->emplace_back(det->id, transform);
		out_corners->emplace_back(det->id, Eigen::Map<Eigen::Matrix<double, 2, 4>>((double*)det->p).cast<float>());
	}

	apriltag_detections_destroy(detections);
}

void DrawAprilDetection(
	cv::Mat& img,
	const int id,
	const Eigen::Matrix<float, 2, 4>& corners) {

	// Draw outline
	cv::line(img,
		cv::Point(corners(0, 0), corners(1, 0)),
		cv::Point(corners(0, 1), corners(1, 1)),
		cv::Scalar(0, 0, 255, 255), 2);
	cv::line(img,
		cv::Point(corners(0, 0), corners(1, 0)),
		cv::Point(corners(0, 3), corners(1, 3)),
		cv::Scalar(0, 255, 0, 255), 2);
	cv::line(img,
		cv::Point(corners(0, 1), corners(1, 1)),
		cv::Point(corners(0, 2), corners(1, 2)),
		cv::Scalar(255, 0, 0, 255), 2);
	cv::line(img,
		cv::Point(corners(0, 2), corners(1, 2)),
		cv::Point(corners(0, 3), corners(1, 3)),
		cv::Scalar(255, 0, 0, 255), 2);

	// Print tag ID
	const int fontface = cv::FONT_HERSHEY_SCRIPT_SIMPLEX;
	const double fontscale = 1.0;
	int baseline;
	auto text = std::to_string(id);
	cv::Size textsize = cv::getTextSize(text, fontface, fontscale, 2, &baseline);
	cv::putText(img, text,
		cv::Point(
			corners.row(0).mean() - textsize.width / 2,
			corners.row(1).mean() + textsize.height / 2),
		fontface, fontscale, cv::Scalar(255, 153, 0, 255), 2);
}

void DrawPose2D(
	cv::Mat& cvMat,
	const Eigen::Matrix4f& transform,
	const CameraParams& intrinsics) {

	// Length of axis [mm]
	const float axisLength = 150.0f;

	// Colors of XYZ axes
	const cv::Scalar axesColors[3] = {
		cv::Scalar(0, 0, 255, 255),    // red
		cv::Scalar(0, 255, 0, 255),    // blue
		cv::Scalar(255, 0, 0, 255) };  // green

	// List of points
	float pts[12]{
		0.0f, 0.0f, 0.0f,        // origin
		axisLength, 0.0f, 0.0f,  // x
		0.0f, axisLength, 0.0f,  // y
		0.0f, 0.0f, axisLength   // z
	};

	// Apply transform to points
	Eigen::Map<Eigen::Matrix<float, 3, 4>> _pts(pts);
	Eigen::Matrix<float, 3, 4> axes = Eigen::Affine3f(transform) * _pts;

	// Project points 3D to 2D
	std::vector<cv::Point2f> axisProjections(4);
	cv::projectPoints(
		cv::Mat(4, 3, CV_32F, (void*)axes.data()),
		cv::Vec3f(),
		cv::Vec3f(),
		intrinsics.cameraMatrix,
		intrinsics.distortionCoefficients,
		axisProjections);

	// Define a render order which places the furthest arms of the coordinate system behind the nearer arms
	uint8_t renderOrder[] = { 1,2,3 };
	std::sort(renderOrder, renderOrder + 3,
		[&axes](uint lhs, uint rhs) {
			auto p1 = axes(2, lhs); // z component of col_1
	auto p2 = axes(2, rhs); // z component of col_2
	return p1 > p2;
		});

	for (auto i : renderOrder) {
		cv::arrowedLine(
			cvMat,
			axisProjections[0],
			axisProjections[i],
			axesColors[i - 1],
			2,
			cv::LINE_AA);
	}
}

std::string FormatTransformToString(
	const Eigen::Matrix4f& transform) {

	std::ostringstream ss;

	ss.setf(ios::fixed);
	ss.setf(ios::showpoint);
	ss.precision(8);

	const float* p = transform.data();
	for (auto i = 0; i < 15; ++i) {
		ss << p[i] << ',';
	}
	ss << p[15] << '\n';

	return ss.str();
}