#pragma once

struct CameraParams {
	cv::Mat cameraMatrix;
	cv::Mat distortionCoefficients;
};

template<typename Derived>
struct DetectionResult {

	DetectionResult(int _id, Derived _matrix) : id(_id), matrix(_matrix) { };
	int id;
	Derived matrix;
};

int EnableK4ACamera(
	k4a_device_t* device,
	k4a_calibration_t* device_transform);

bool InitK4A(
	k4a_device_t device,
	k4a_calibration_t device_calibration,
	k4a_transformation_t* device_transform,
	k4a_image_t* xy_buffer,
	k4a_image_t* rgbd_buffer);

void CreateAprilDetector(
	apriltag_family_t** tf,
	apriltag_detector_t** td);

void ProcessColorImage(
	k4a_capture_t& capture, 
	cv::Mat& img);

void ProcessIRImage(
	k4a_capture_t& capture, 
	cv::Mat& img_ir,
	cv::Mat& img_tof,
	bool equalise_ir = true);

int ProcessPointCloud(
	k4a_capture_t& capture,
	k4a_transformation_t transformation,
	k4a_image_t xyBuffer,
	k4a_image_t rgbdBuffer,
	pcl::PointCloud<pcl::PointXYZRGB >::Ptr pcl_cloud);

void DetectAprilTag3D(
	apriltag_detector_t* td,
	cv::Mat& img,
	CameraParams& intrinsics,
	std::vector<DetectionResult<Eigen::Matrix<float, 2, 4>>>* out_corners,
	std::vector<DetectionResult<Eigen::Matrix4f>>* out_detections);

void DrawAprilDetection(
	cv::Mat& img,
	const int id,
	const Eigen::Matrix<float, 2, 4>& corners);

void DrawPose2D(
	cv::Mat& cvMat,
	const Eigen::Matrix4f& transform,
	const CameraParams& intrinsics);

void DrawPose3D(
	pcl::visualization::PCLVisualizer::Ptr visualizer,
	Eigen::Matrix4f pose,
	int id);

std::string CreateDataFolder(
	std::string folder_name,
	bool add_timestamp,
	k4a_device_t device,
	k4a_calibration_t device_Calibration);

void LogData(
	std::string folder,
	cv::Mat rgb,
	cv::Mat ir,
	cv::Mat tof,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud,
	std::vector<DetectionResult<Eigen::Matrix4f>> detections_rgb,
	std::vector<DetectionResult<Eigen::Matrix4f>> detections_depth);

std::string FormatTransformToString(
	const Eigen::Matrix4f& transform);

