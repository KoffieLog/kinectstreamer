#pragma once

// Suppress an error in apriltag library
#define _CRT_SECURE_NO_WARNINGS
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS

// Standard libraries
#include <filesystem> 
#include <iostream>
#include <fstream>
#include <map>

// Project includes
#include "kinect_helper_funcitons.h"
#include "HistEQ.h"
#include "AsyncKeyListener.h"

// Image processing libraries
#include <k4a/k4a.h>
#include <opencv2/core.hpp>
#include <opencv2/core/utils/logger.hpp>

#include <pcl/common/common.h>
#include <pcl/visualization/cloud_viewer.h>

#include <apriltag/apriltag.h>
#include <apriltag/tag36h11.h>