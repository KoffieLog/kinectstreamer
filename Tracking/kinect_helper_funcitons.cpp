#include "pch.h"
#include "kinect_helper_funcitons.h"

#pragma region K4A Library Helpers

void create_xy_table(
	const k4a_calibration_t* calibration,
	k4a_image_t xy_table,
	int* valid_size)
{
	k4a_float2_t* table_data = (k4a_float2_t*)(void*)k4a_image_get_buffer(xy_table);

	const int width = calibration->depth_camera_calibration.resolution_width;
	const int height = calibration->depth_camera_calibration.resolution_height;

	k4a_float2_t p;
	k4a_float3_t ray;
	int valid;
	int count = 0;

	for (int y = 0, idx = 0; y < height; y++)
	{
		p.xy.y = (float)y;
		for (int x = 0; x < width; x++, idx++)
		{
			p.xy.x = (float)x;

			// Project pixel [u,v] to unit plane [x,y,z=1]
			k4a_calibration_2d_to_3d(
				calibration, &p, 1.f, K4A_CALIBRATION_TYPE_DEPTH, K4A_CALIBRATION_TYPE_DEPTH, &ray, &valid);

			if (valid)
			{
				table_data[idx].xy.x = ray.xyz.x;
				table_data[idx].xy.y = ray.xyz.y;
				count++;
			}
			else
			{
				table_data[idx].xy.x = std::numeric_limits<float>::quiet_NaN();
				table_data[idx].xy.y = std::numeric_limits<float>::quiet_NaN();
			}
		}
	}

	*valid_size = count;
}

#pragma endregion

#pragma region PointCloudLibrary Helpers

int generate_pcl_cloud(
	const k4a_image_t depth_image,
	const k4a_image_t xy_table,
	const k4a_image_t opt_color_map,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud,
	bool isStructured) {

	// pointers for depth and undistortion data
	const uint16_t* pDepth = (uint16_t*)k4a_image_get_buffer(depth_image);      // pointer to tof data
	const k4a_float2_t* pLUT = (k4a_float2_t*)k4a_image_get_buffer(xy_table);   // pointer to undistort lookup table 

	// pointer to optional color map
	const bgra_t* pColor = opt_color_map != NULL ? 
		(bgra_t*)k4a_image_get_buffer(opt_color_map) :
		nullptr; 

	int width = k4a_image_get_width_pixels(depth_image);
	int height = k4a_image_get_height_pixels(depth_image);
	int valid_count = 0;

	if (isStructured) {

		// Iterate data and keep any sparse structure
		for (auto i = 0; i < (width * height); ++i) {

			if (pDepth[i] != 0 && !isnan(pLUT[i].xy.x) && !isnan(pLUT[i].xy.y))
			{
				// Set position
				point_cloud->points[i].x = pLUT[i].xy.x * (float)pDepth[i];
				point_cloud->points[i].y = pLUT[i].xy.y * (float)pDepth[i];
				point_cloud->points[i].z = (float)pDepth[i];

				// Set color if available
				if (pColor) {
					point_cloud->points[i].b = pColor[i].b;
					point_cloud->points[i].g = pColor[i].g;
					point_cloud->points[i].r = pColor[i].r;
				}
				// Default is white
				else {
					point_cloud->points[i].b = 255;
					point_cloud->points[i].g = 255;
					point_cloud->points[i].r = 255;
				}

				valid_count++;
			}
			else {
				point_cloud->points[i].x = std::numeric_limits<float>::quiet_NaN();
				point_cloud->points[i].y = std::numeric_limits<float>::quiet_NaN();
				point_cloud->points[i].z = std::numeric_limits<float>::quiet_NaN();
			}
		}
	}
	else {

		// Iterate data and pack valid points in front of point cloud
		for (auto i = 0; i < (width * height); ++i) {

			if (pDepth[i] != 0 && !isnan(pLUT[i].xy.x) && !isnan(pLUT[i].xy.y))
			{
				// Set position
				point_cloud->points[valid_count].x = pLUT[i].xy.x * (float)pDepth[i];
				point_cloud->points[valid_count].y = pLUT[i].xy.y * (float)pDepth[i];
				point_cloud->points[valid_count].z = (float)pDepth[i];

				// Set color if available
				if (pColor) {
					point_cloud->points[valid_count].b = pColor[i].b;
					point_cloud->points[valid_count].g = pColor[i].g;
					point_cloud->points[valid_count].r = pColor[i].r;
				}
				// Default is white
				else {
					point_cloud->points[valid_count].b = 255;
					point_cloud->points[valid_count].g = 255;
					point_cloud->points[valid_count].r = 255;
				}

				valid_count++;
			}
		}

		// nan rest of points
		for (auto i = valid_count; i < (width * height); ++i) {
			point_cloud->points[i].x = std::numeric_limits<float>::quiet_NaN();
			point_cloud->points[i].y = std::numeric_limits<float>::quiet_NaN();
			point_cloud->points[i].z = std::numeric_limits<float>::quiet_NaN();
		}
	}
	

	return valid_count;
}

#pragma endregion

#pragma region OpenCV Helpers

cv::Mat GetCameraMatrixFromK4A(
	const k4a_calibration_camera_t cal) {

	auto intrinsics = cal.intrinsics.parameters.param;
	auto fx = intrinsics.fx;
	auto fy = intrinsics.fy;
	auto cx = intrinsics.cx;
	auto cy = intrinsics.cy;

	cv::Mat cameraMatrix = (cv::Mat_<float>(3, 3) <<
		fx,   0.0f, cx,
		0.0f, fy,   cy,
		0.0f, 0.0f, 1.0);

	return cameraMatrix;
}

cv::Mat GetDistortionMatrixFromK4A(
	const k4a_calibration_camera_t cal) {

	auto intrinsics = cal.intrinsics.parameters.param;

	auto k1 = intrinsics.k1;
	auto k2 = intrinsics.k2;
	auto k3 = intrinsics.k3;
	auto k4 = intrinsics.k4;
	auto k5 = intrinsics.k5;
	auto k6 = intrinsics.k6;
	auto p1 = intrinsics.p1;
	auto p2 = intrinsics.p2;

	cv::Mat distortionMatrix = (cv::Mat_<float>(8, 1) <<
		k1, k2, p1, p2, k3, k4, k5, k6);

	return distortionMatrix;
}

void InitUndistortionMapsCV(
	const k4a_calibration_camera_t cal,
	cv::Mat& xMap,
	cv::Mat& yMap) {

	// Wrap kinect calibration struct into opencv mats
	auto cameraMatrix = GetCameraMatrixFromK4A(cal);
	auto distortionMatrix = GetDistortionMatrixFromK4A(cal);

	// Populate x,y maps
	cv::initUndistortRectifyMap(
		cameraMatrix,
		distortionMatrix,
		cv::Mat::eye(3, 3, CV_32F),
		cameraMatrix,
		cv::Size(cal.resolution_width, cal.resolution_height),
		CV_32FC1,
		xMap,
		yMap);
}

int MapDepthToPointCloudCV(
	const cv::Mat& depthMap,
	const k4a_calibration_camera_t calibration,
	float* cloudBuffer) {

	// Unpack calibration vars
	std::size_t height = calibration.resolution_height;
	std::size_t width = calibration.resolution_width;
	float fx = calibration.intrinsics.parameters.param.fx;
	float fy = calibration.intrinsics.parameters.param.fy;
	float cx = calibration.intrinsics.parameters.param.cx;
	float cy = calibration.intrinsics.parameters.param.cy;

	int n = 0;
	for (auto h = 0; h < height; ++h) {

		uint16_t* pRow = (uint16_t*)(void*)depthMap.ptr(h);  // pointer to data in row h

		for (auto w = 0; w < width; ++w) {

			// check for zero depth info at pixel
			if (pRow[w] == 0)
				continue;

			float z = pRow[w];
			float x = (w - cx) * z / fx;
			float y = (h - cy) * z / fy;

			cloudBuffer[3 * n + 0] = x;
			cloudBuffer[3 * n + 1] = y;
			cloudBuffer[3 * n + 2] = z;
			n++;
		}
	}

	// Return size of new cloud data
	return n;
}

cv::Mat ImageKinect2CV(
	const k4a_image_t kinect_image) {

	if (kinect_image == nullptr)
		return cv::Mat::eye(32,32,CV_32F);

	return cv::Mat(
		k4a_image_get_height_pixels(kinect_image), 
		k4a_image_get_width_pixels(kinect_image), 
		TYPE_K4A_TO_CV.at(k4a_image_get_format(kinect_image)),
		(void*)k4a_image_get_buffer(kinect_image),
		k4a_image_get_stride_bytes(kinect_image));
}

#pragma endregion

#pragma region Eigen Helpers

void TransformMatrix(
	Eigen::Matrix4f& transform,
	k4a_calibration_t& device_calibration,
	k4a_calibration_type_t from,
	k4a_calibration_type_t to) {

	// Transform from A
	Eigen::Matrix4f A = Eigen::Matrix4f::Identity();
	A.block<3, 3>(0, 0) = Eigen::Map<Eigen::Matrix3f>(device_calibration.extrinsics[from]->rotation).transpose();
	A.block<3, 1>(0, 3) = Eigen::Map<Eigen::Vector3f>(device_calibration.extrinsics[from]->translation);

	// Transform to B
	Eigen::Matrix4f B = Eigen::Matrix4f::Identity();
	B.block<3, 3>(0, 0) = Eigen::Map<Eigen::Matrix3f>(device_calibration.extrinsics[to]->rotation).transpose();
	B.block<3, 1>(0, 3) = Eigen::Map<Eigen::Vector3f>(device_calibration.extrinsics[to]->translation);

	// Apply transform from a to b
	transform = B.inverse() * A * transform;
}

#pragma endregion
