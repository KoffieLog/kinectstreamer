/// Taylor Frantz
/// ETRO 2023
/// Helper class for asynchronous keyboard input, linking keys to callback funcitons
/// 

#pragma once
#include <Windows.h>
#include <thread>
#include <mutex>
#include <vector>

class AsyncKeyListener {

public:

	AsyncKeyListener() {
		listener = new std::thread(ListenerLoop, this);
	}

	// Assigns a callback funciton to a specific key code
	// See: https://learn.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
	void RegisterCallback(
		uint8_t virtual_key_code,
		std::function<void()> callback) {

		std::lock_guard<std::mutex> lock(vector_mutex);
		registered_keys.emplace_back(virtual_key_code, callback);
	}

	void UnRegisterCallback(
		uint8_t virtual_key_code) {

		std::lock_guard<std::mutex> lock(vector_mutex);
		registered_keys.erase(
			std::remove_if(
				registered_keys.begin(),
				registered_keys.end(),
				[virtual_key_code](const KeyStateCallback& c) { return c.code == virtual_key_code; }),
			registered_keys.end());
	}

	~AsyncKeyListener() {

		if (listener) {
			exit = true;
			listener->join();
			delete listener;
		}
	}

private:

	struct KeyStateCallback {

		KeyStateCallback(int _code, std::function<void()> _callback) : code(_code), callback(_callback), isPressed(false) { };

		int code;
		bool isPressed;
		std::function<void()> callback;
	};

	static void ListenerLoop(
		AsyncKeyListener* listener) {

		while (!listener->exit) {

			// Lock vector
			listener->vector_mutex.lock();

			for (auto& key : listener->registered_keys) {

				// is_pressed will be true if the key is currently being held down
				bool is_pressed = (GetAsyncKeyState(key.code) & 0x8000) != 0;

				// check transition from not pressed to pressed
				if (!key.isPressed && is_pressed) {
					key.callback();
				}

				key.isPressed = is_pressed;
			}

			// Release vector lock and sleep
			listener->vector_mutex.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(200));
		}
	}

private:
	bool exit = false;
	std::thread* listener = nullptr;
	std::vector<KeyStateCallback> registered_keys;
	std::mutex vector_mutex;
};