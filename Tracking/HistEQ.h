#pragma once


template<typename T, int BINS, T LOWER, T UPPER>
void Percentile(
	const T* pData,
	const int nData,
	int percLower,
	int percUpper,
	T* lower,
	T* upper) {

	// Histogram
	unsigned histogram[BINS] = {};
	unsigned sumHist = 0;
	float binWidth = (float)BINS / (std::numeric_limits<T>::max() + 1);
	for (unsigned i = nData; i--;) {

		// Test range
		bool inRange = pData[i] >= LOWER && pData[i] < UPPER;

		// Update histogram (round value to nearest bin index)
		if (inRange) {
			histogram[(unsigned)(pData[i] * binWidth + 0.5f)]++;
			sumHist++;
		}
	}

	// Lower percentile
	if (lower) {

		int index = 0;
		int thresh = sumHist * percLower * 0.01 + 0.5;
		while (thresh > 0) {
			thresh -= histogram[index++];
		}
		*lower = index / binWidth;
	}

	// Upper percentile
	if (upper) {

		// If pixels are lower intensity, move forwards through histogram 
		int index = 0;
		int thresh = sumHist * percUpper * 0.01 + 0.5;
		while (thresh > 0) {
			thresh -= histogram[index++];
		}
		*upper = index / binWidth;
	}
}

template<typename ImageDateType, ImageDateType MIN, ImageDateType MAX>
void MinMax(
	const ImageDateType* pData, 
	const size_t nData, 
	ImageDateType* min, 
	ImageDateType* max) {

	ImageDateType _min = pData[0];
	ImageDateType _max = pData[0];

	for (size_t i = 1; i < nData; i++) {
		if (max && pData[i] < MAX && pData[i] > _max) { _max = pData[i]; };
		if (min && pData[i] < MIN && pData[i] < _min) { _min = pData[i]; };
	}

	if (max)
		*max = _max;

	if (min)
		*min = _min;
}
