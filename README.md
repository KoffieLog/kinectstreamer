# KinectStreamer
Super simple app for azure kinect dk camera

## Demonstrates
- [Kinect SDK](https://github.com/microsoft/Azure-Kinect-Sensor-SDK)
- [OpenCV](https://github.com/opencv/opencv) integration
- [PCL](https://github.com/PointCloudLibrary/pcl) integration
- [AprilTag](https://github.com/AprilRobotics/apriltag) detection


## Installation

```
cd <your most favourite directory>
git clone https://gitlab.com/KoffieLog/kinectstreamer.git
```

For dependencies, it is reccomended to use [VCPKG](https://vcpkg.io/en/getting-started)
```
vcpkg install opencv[core,world,contrib]:x64-windows
vcpkg install pcl[visualization]:x64-windows
vcpkg install azure-kinect-sensor-sdk:x64-windows
```
Notes:  
- PCL is built with the optional [visualizer](https://pcl.readthedocs.io/projects/tutorials/en/latest/pcl_visualizer.html) library.
- OpenCV is built with the optional [contrib](https://github.com/opencv/opencv_contrib) library. Also all library DLLs are wrapped into a single OpenCVworld.dll rather than populated individually.
- The above builds libraries for dynamic linkage, append the *static* flag if that's more your bag.
- AprilTag library does not have a vcpkg triplet at this time, use [cmake](https://cmake.org/).

## The Program

The program opens a conenction the first attached kinect camera<br/>
Three wildows will appear
- RGB sensor stream (no changes)<br/>
- Infrared sensor stream (contrast enhanced)<br/>
- 3D point coud viewer<br/>
![image](resources/ui_windows.PNG){width=66%}

Press "r" starts/stops logging data to device<br/>
Data will be logged into ```/Data_[timestamp_in_ms]```<br/>
Each set of files will be timestamped with milliseconds<br/>
![image](resources/log_files.PNG){width=25%}

The file ```\Data\device_info.json``` contains calibtation data for the kinect senspors
![image](resources/example_device_info.PNG){width=66%}

Press "esc"" to close program<br/> 

